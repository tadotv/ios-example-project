//
//  PlayerViewController.swift
//  iOS Example Project
//
//  Created by Gema Megantara on 30/09/19.
//  Copyright © 2019 TADOtv. All rights reserved.
//

import UIKit
import WebKit

class PlayerViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var btnBack: UIButton!
    var webView: WKWebView!
    var urlVideo: String!
    var timer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let webConfiguration = WKWebViewConfiguration()
        // enable javascript, remove need user interaction for media, inline media playback
        webConfiguration.preferences.javaScriptEnabled = true
        webConfiguration.mediaTypesRequiringUserActionForPlayback = []
        webConfiguration.allowsInlineMediaPlayback = true
        
        self.webView = WKWebView(frame: view.bounds, configuration: webConfiguration)
        
        // adjust inset so webview will be fullscreen
        self.webView.scrollView.contentInsetAdjustmentBehavior = .never;
        
        // add webview as subview
        self.view.addSubview(self.webView)
        
        // set url on web view
        let url = URL(string: urlVideo)!
        self.webView.load(URLRequest(url: url))
        
        //add tap selector on web view
        let tapPress = UITapGestureRecognizer(target: self, action: #selector(self.tapPressed))
        tapPress.delegate = self
        self.webView.addGestureRecognizer(tapPress)
        
        //trigger tap pressed
        self.tapPressed()
        
        self.view.bringSubviewToFront(self.btnBack)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.webView.frame = view.bounds
    }

    @IBAction func btnBackTapped(_ sender: UIButton) {
        //set orientation back to portrait
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapPressed()
    {
        //animate show back button
        UIView.animate(withDuration: 0.3, animations: {
            if (self.timer != nil){
                self.timer.invalidate()
                self.timer = nil
            }
            
            self.btnBack.alpha = 1
            self.view.layoutIfNeeded()
        }) { (action) in
            //set timer 2 seconds to hide back button
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.hideBackButton), userInfo: nil, repeats: false)
        }
    }
    
    @objc func hideBackButton(){
        //animate hide back button
        UIView.animate(withDuration: 0.3) {
            self.btnBack.alpha = 0
            self.view.layoutIfNeeded()
        }
    }
    
}

extension PlayerViewController: UIGestureRecognizerDelegate {
    //this is required, to enable detect tap gesture
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
