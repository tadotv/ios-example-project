//
//  ListViewController.swift
//  iOS Example Project
//
//  Created by Gema Megantara on 30/09/19.
//  Copyright © 2019 TADOtv. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        
        //set modal as fullscreen
        viewController.modalPresentationStyle = .fullScreen
        
        //set url video for vertical
        viewController.urlVideo = "https://botolkecap.tadotv.com/watch/PGKulsG"
        
        //vertical mode
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.myOrientation = .portrait
        
        if (indexPath.row == 1){
            //horizontal mode
            appDelegate.myOrientation = .landscape
            
            //set url video for horizontal
            viewController.urlVideo = "https://botolkecap.tadotv.com/watch/makmur"
        }
        
        //present the controller
        self.present(viewController, animated: true, completion: nil)
    }

}
