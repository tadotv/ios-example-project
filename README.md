# iOS Example Project #

This an example project about embedding TADOtv Web Player inside a WKWebView object in iOS application.

## Getting Started ##

### How to Use TADOtv Web Player URL on WKWebView ###

It's as simple as this:

```
var webView: WKWebView!

// enable javascript, remove need user interaction for media, inline media playback in WKWebViewConfiguration
let webConfiguration = WKWebViewConfiguration()

webConfiguration.preferences.javaScriptEnabled = true
webConfiguration.mediaTypesRequiringUserActionForPlayback = []
webConfiguration.allowsInlineMediaPlayback = true

// construct the webview
self.webView = WKWebView(frame: view.bounds, configuration: webConfiguration)

// adjust inset so webview will be fullscreen, especially for iPhone X family
self.webView.scrollView.contentInsetAdjustmentBehavior = .never;

// add webview as subview
self.view.addSubview(self.webView)

// set url on web view
let url = URL(string: urlVideo)!
self.webView.load(URLRequest(url: url))
```

### Handling Vertical & Horizontal TADOtv Video ###

There are two types of video in TADOtv: 

* Vertical
* Horizontal

To handle both video, we have to set correct orientation for each video.

### Handle Vertical Video ###

Use below code to set orientation to vertical/portrait.

```
//vertical mode
let appDelegate = UIApplication.shared.delegate as! AppDelegate
appDelegate.myOrientation = .portrait
```

### Handle Horizontal Video ###

Use below code to set orientation to horizontal/landscape.

```
//horizontal mode
let appDelegate = UIApplication.shared.delegate as! AppDelegate
appDelegate.myOrientation = .landscape
```

**IMPORTANT NOTE:**

If you want to set orientation back to portrait, after playing horizontal video. Before dismissing the controller, call the set orientation code first:
```
//vertical mode
let appDelegate = UIApplication.shared.delegate as! AppDelegate
appDelegate.myOrientation = .portrait
```

and because changing orientation, webview have to re-adjust for the width and height value, so we have to override viewDidLayoutSubviews method and set again the webview frame:
```
override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    self.webView.frame = view.bounds
}
```